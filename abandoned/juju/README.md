In order to port juju it looks like we need to be able to build and utilize snapd. The make install-dependencies step found on

https://github.com/juju/juju/blob/develop/CONTRIBUTING.md

errored out while attempting to build snap resources. This is likely why canonical suggests from the get go to simply install juju with snap itself.

We might find information on building snapd here

https://snapcraft.io/docs/installing-snapd

If we're able to build it from source we should be able to just install juju with snapd itself and not have to worry about an official alpine package, though a simple apk add juju would be nice long term.