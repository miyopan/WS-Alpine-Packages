# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=ntopng
pkgver=4.2
pkgrel=0
pkgdesc="ntop next-generation"
url="http://www.ntop.org"
# luajit is not available for disabled arches
arch="all !s390x"
license="GPL"
depends=
pkgusers="ntop"
pkggroups="ntop"
makedepends="autoconf automake curl-dev geoip-dev glib-dev hiredis-dev readline-dev
	libpcap-dev libtool libxml2-dev lua-dev luajit-dev lua-redis libressl-dev
	paxmark rrdtool-dev sqlite-dev wget zeromq-dev zlib-dev libmaxminddb-dev
	linux-headers mariadb-connector-c-dev git json-c json-c-dev"
install="$pkgname.pre-install"
#subpackages="$pkgname-doc"
source="ntopng-$pkgver.tar.gz::https://github.com/ntop/ntopng/archive/$pkgver.tar.gz
	$pkgname.initd
	$pkgname.confd
	ntopng-update-geoip-db
	"

build() {
	git clone https://github.com/ntop/nDPI.git
	cd nDPI
	./autogen.sh
	make
	cd ..
	./autogen.sh
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var/lib \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make all
}

package() {
	make DESTDIR="$pkgdir" MAN_DIR="$pkgdir/usr/share" install || return 1

	install -m755 -D "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
	install -m755 -D "$srcdir"/ntopng-update-geoip-db \
	 "$pkgdir"/usr/bin/ntopng-update-geoip-db
	# ntop internal db dir
	install -d -o ntop -g ntop -m755 "$pkgdir"/var/lib/ntopng/geoip || return 1
	install -d -o ntop -g ntop -m755 "$pkgdir"/var/run/ntopng || return 1
	# need to disable PAX mprotect protection for luajit
	paxmark -m "$pkgdir"/usr/bin/$pkgname
}

sha512sums="e89ec3f35644ae8bc570d9802ecfadbf0067145e23a589df2032e8781e9c954f4166d3abb650ddafc65e4fbad656730ee1abf9c5273d3e09f8173d423436b8ff  ntopng-4.2.tar.gz
b7924953953470971e67e463260514c30a4a3038ecbb642aec1ad66d12b5af76bdda56c99de675a1fbb106664fa0e3779a4f35fdeedc94d65af486053b7650e1  ntopng.initd
bbe7a15e0aec59f12264f39bf33abb67a12b76adac3a6554fcf1e21849aea06fce1e0c0d9831836bd46105a5cc4b3eb780866934d8711386e1cb1218dbdbbe8b  ntopng.confd
de509706c36e895159b4e57b5fe53f1fa8f32167416aad5fa391ac107f0e7e1a06fa5ce6d92816ef7cb13bf98bb74cb99fa482d03f77a6beb38eafe1d53d2deb  ntopng-update-geoip-db"
