# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Contributor: Benjamin Buccianti <bebuccianti@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=nyxt
pkgver=2.2.2
pkgrel=0
pkgdesc="Atlas Engineer Nyxt Browser"
url="https://nyxt.atlas.engineer"
arch="x86 x86_64 aarch64"
#SBCL must be compiled with thread support for Nyxt to work
#SBCL thread is not supported on armv7
#SBCL is only built on arches x86, x86_64, aarch64, and armv7
license="BSD-3-Clause"
source="$pkgname-$pkgver.tar.xz::https://github.com/atlas-engineer/nyxt/releases/download/$pkgver/nyxt-$pkgver-source-with-submodules.tar.xz"
depends="
	glib-networking
	gsettings-desktop-schemas
	libfixposix-dev
	gstreamer
	gst-plugins-base
	gst-plugins-good
	gst-plugins-bad
	gst-plugins-ugly
	libffi-dev
	gobject-introspection
	webkit2gtk
	"
makedepends="git sbcl webkit2gtk-dev"
options="!strip net" #Stripping the package causes the package to build,
# but the nyxt browser is unfunctional, dropping to an SBCL repl
# instead of properly running.
builddir="$srcdir"

replaces=next
provides=next=$pkgver-r$pkgrel

prepare() {
	default_prepare
	git submodule update --init --force
}

build() {
	make all
}

check() {
	make check
}

package() {
	make PREFIX=/usr DESTDIR="$pkgdir" install
}

sha512sums="
0c809f36a4a4b880518cf508e77b6638376c4e429c9bbb74461a3e3943197552d0a689532e4ab28f5962166baef8f771ac4aee19b980f141987dbd4c0f10a3e9  nyxt-2.2.2.tar.xz
"
