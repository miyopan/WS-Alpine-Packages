# Contributor: "Will Sinatra <wpsinatra@gmail.com>"
# Maintainer: "Will Sinatra <wpsinatra@gmail.com>"
pkgname="google-cloud-sdk"
pkgver=291.0.0
pkgrel=0
pkgdesc="A set of command-line tools for the Google Cloud Platform."
url="https://cloud.google.com/sdk/"
license="Apache"
arch="noarch"
makedepends="python2"
depends="python3 py3-crcmod"
source="https://dl.google.com/dl/cloudsdk/release/downloads/for_packagers/linux/google-cloud-sdk_$pkgver.orig.tar.gz
	google-cloud-sdk.sh"
options="!strip staticlibs"
builddir="$srcdir/$pkgname"
#Every tool has a sub package, this needs to be expanded
#subpackages="google-cloud-sdk-doc"

package() {
	# The Google code uses a _TraceAction() method which spams the screen even
	# in "quiet" mode, we're throwing away output on purpose to keep it clean	#  ref: lib/googlecloudsdk/core/platforms_install.py
	python "$builddir/bin/bootstrapping/install.py" \
		--quiet \
		--usage-reporting False \
		--path-update False \
		--bash-completion False \
		--additional-components "" \
	1 > /dev/null

	install -D -m 0644 "$srcdir/google-cloud-sdk.sh" \
		"$pkgdir/etc/profile.d/google-cloud-sdk.sh"

	install -D -m 0644 "$builddir/completion.bash.inc" \
		"$pkgdir/etc/bash_completion.d/google-cloud-sdk"

	install -D -m 0644 "$builddir/completion.zsh.inc" \
    	"$pkgdir/usr/share/zsh/site-functions/_gcloud"
		
	#There's a man file for every single tool, this won't work so we need to find a way to copy those out and provide them as individually installable files. Probably need to speak with someon in IRC about it
	#install -D -m 0755 "$builddir/help/man/man1" \
	#	"$pkgdir/usr/share/man/man1"

	install -D -m 0755 "$builddir/bin/anthoscli" \
		"$pkgdir/usr/bin/anthoscli"

	install -D -m 0755 "$builddir/bin/bq" \
		"$pkgdir/usr/bin/bq"

	install -D -m 0755 "$builddir/bin/docker-credential-gcloud" \
		"$pkgdir/usr/bin/docker-credential-gcloud"

	install -D -m 0755 "$builddir/bin/gcloud" \
		"$pkgdir/usr/bin/gcloud"

	install -D -m 0755 "$builddir/bin/gsutil" \
		"$pkgdir/usr/bin/gsutil"

	#Pretty sure this is just the deps folder, but marking it here to check later on
	#install -D -m 0755 "$builddir/bin/bootstrapping" \
	#	"$pkgdir/usr/bin/bootstrapping"
}

sha512sums="d16fe78550a986713052611a92b975074c7200ef4531cb9d57fe06a7c084d6837e144112e0fb21e186e2a0def2dca7bb4123247eb4fc6e2dc5b9da9beb4fdc5b  google-cloud-sdk_291.0.0.orig.tar.gz
42ded10f19f5ec5fb91cdbe452db94529954a86386f8ed68baf8b42ee71a6b2c2a86ad4b7a1973431d226b742834803a8783441605b20a3a1aae197c070ac733  google-cloud-sdk.sh"
